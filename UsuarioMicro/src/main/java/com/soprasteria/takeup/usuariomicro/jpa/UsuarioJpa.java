package com.soprasteria.takeup.usuariomicro.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.soprasteria.takeup.usuariomicro.dto.Usuario;
import com.soprasteria.takeup.usuariomicro.interfacesDao.IUsuarioDao;
import com.soprasteria.takeup.usuariomicro.mappers.UsuarioMapper;

@Repository
public class UsuarioJpa implements IUsuarioDao{
	
	@Autowired
	UsuarioRepositorio usuarioRepo;

	@Override
	public int guardar(Usuario usuario) {
		usuarioRepo.save(UsuarioMapper.toEntity(usuario));
		return 0;
	}

}
