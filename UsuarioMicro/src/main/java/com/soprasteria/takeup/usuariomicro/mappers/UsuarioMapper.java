package com.soprasteria.takeup.usuariomicro.mappers;

import com.soprasteria.takeup.usuariomicro.entities.Usuario;

public class UsuarioMapper {

	public static Usuario toEntity(com.soprasteria.takeup.usuariomicro.dto.Usuario u) {
		
		if (u!=null) {
			Usuario user= new Usuario();
			user.setIdUsuario(u.getIdUsuario());
			user.setNombre(u.getNombre());
			user.setContrasena(u.getContrasena());
			return user;
		}else
			return null;
	}
	
}
