package com.soprasteria.takeup.usuariomicro.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.soprasteria.takeup.usuariomicro.entities.Usuario;

public interface UsuarioRepositorio extends JpaRepository<Usuario,Long>{
	
	
	
}
