package com.soprasteria.takeup.usuariomicro.interfacesDao;

import com.soprasteria.takeup.usuariomicro.dto.Usuario;

public interface IUsuarioDao {

	public int guardar(Usuario usuario);
}
