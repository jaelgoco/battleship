package com.soprasteria.takeup.usuariomicro.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.soprasteria.takeup.usuariomicro.dto.Usuario;
import com.soprasteria.takeup.usuariomicro.interfacesControllers.IUsuario;

@RestController
public class UsuariosRestController {

	@Autowired
	IUsuario inUsuario;
	
	@PostMapping("/alta")
	public int alta(@RequestBody Usuario usuario) {
		System.out.println("entro aqui");
		return inUsuario.alta(usuario);
	} 
}
