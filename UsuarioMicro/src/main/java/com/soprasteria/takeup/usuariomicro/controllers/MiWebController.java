package com.soprasteria.takeup.usuariomicro.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MiWebController {

	@GetMapping ("/")
	public String init () {
		
		return "login";
	}
	
	@GetMapping ("/gameScreen")
	public String init1 () {
		
		return "gameScreen";
	}
	
}
