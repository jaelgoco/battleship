package com.soprasteria.takeup.usuariomicro.interfacesControllers;

import com.soprasteria.takeup.usuariomicro.dto.Usuario;

public interface IUsuario {

	public int alta(Usuario usuario);
	
}
