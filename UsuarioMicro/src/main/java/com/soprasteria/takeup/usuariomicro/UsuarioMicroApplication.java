package com.soprasteria.takeup.usuariomicro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsuarioMicroApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsuarioMicroApplication.class, args);
	}

}
