package com.soprasteria.takeup.usuariomicro.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.soprasteria.takeup.usuariomicro.dto.Usuario;
import com.soprasteria.takeup.usuariomicro.interfacesControllers.IUsuario;
import com.soprasteria.takeup.usuariomicro.interfacesDao.IUsuarioDao;

@Service
public class UsuarioSer implements IUsuario{

	@Autowired
	IUsuarioDao interDao;
	
	@Override
	public int alta(Usuario usuario) {
		return interDao.guardar(usuario);
	}

}
